#pragma once

#include <iostream>
using namespace std;

//namespace Developer1{
class CClock				//Declaration of class CClock
{
public:
	const int timeZone = 3;
	void setTime(int, int, int);
	void getTime(int &hours, int &minutes, int &seconds) const{
		hours = hr;
		minutes = min;
		seconds = sec;
	}
	void printTime();
	void incrementSeconds();
	void incrementSeconds(int);
	void incrementMinutes();
	void incrementHours();

	bool equalTime (const CClock&) const;

	CClock(long miliseconds){
		hr = miliseconds / 1000 / 3600;
		min = miliseconds / 1000 / 60;
		sec = miliseconds / 1000 / 1;
	}

	CClock(const int hours=0,const int minutes=0, int seconds=0){
		setTime(hours,minutes,seconds);
	}
	
	~CClock(){
	}
	
private:
	int hr;
	int min;
	int sec;
	
};
//}
