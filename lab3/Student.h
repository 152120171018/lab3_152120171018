#pragma once

#include <string>
using namespace std;

//namespace Developer2{

class Student
{
public:
	void setStudent(string _name, int _id);
	void print();
	int calculateGrade();
	int midTermExam = 40;
	int finalExam = 50;
private:
	string name;
	int id;
};
//}
